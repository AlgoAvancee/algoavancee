# README #

### Mini Projet d'Algorithme Avancée ###

Il consiste à résoudre le problème de triangulation d’un polygone convexe à l’aide de trois algorithmes différents : “essais successifs”, “programmation dynamique” et “glouton”.

   
### Lancer le programme ###

* Sous terminal : 
  1. Ouvrir un terminal puis se placer au répertoir du projet algorithme avancée
  2. Ecrire la commande : java -jar projetAlgo.jar nombreDeSommets

* Paramètre
  nombreDeSommets : c'est le nombre de sommets d'un polygone que vous voulez tester (entre 4 et 15).

Commande exemple : java -jar projetAlgo.jar 10

* Sous eclipse :

File -> Import -> General (Existing Projects into Workspace) -> Directory...(choisir le fichier AlgoAvancee) -> select all -> finish

* Vous pouvez changer le nombre de sommets à tester dans le main de la classe Test en modifiant la valeur 'nbSommets'.

### Description de l'affichage de l'execution ###

Le programme execute dans l'ordre 

* La méthode d'essais successifs sans optimisation
* La méthode d'essais successifs avec l'optimisation
* La méthode de programmation dynamique 
* L'algorithme glouton.

Après avoir executé chaque différente fonction, il affiche 

* Le temps d'execution
* Le résultat de somme de longueurs des cordes tracées 
* Les cordes tracées avec les sommets