package projet;

import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		int nbSommets = 4;
		if (args.length > 0) {
		    try {
		    	nbSommets = Integer.parseInt(args[0]);
		    } catch (NumberFormatException e) {
		        System.err.println("Entier indiquant le nombre de sommets entre 3 et 15, " + args[0] + " n'est pas un entier.");
		        System.exit(1);
		    }
		}
		
		// Sommets à tester
		Sommet sommet0 = new Sommet(0, 0);
		Sommet sommet1 = new Sommet(0,20);
		Sommet sommet2 = new Sommet(20,40);
		Sommet sommet3 = new Sommet(40,30);
		Sommet sommet4 = new Sommet(40,0);
		Sommet sommet5 = new Sommet(30,-20);
		Sommet sommet6 = new Sommet(10,-18);
		Sommet sommet7 = new Sommet(9,-13);
		Sommet sommet8 = new Sommet(8,-12);
		Sommet sommet9 = new Sommet(11,-11);
		Sommet sommet10 = new Sommet(10,-10);
		Sommet sommet11 = new Sommet(9,-9);
		Sommet sommet12 = new Sommet(8,-8);
		Sommet sommet13 = new Sommet(7,-7);
		Sommet sommet14 = new Sommet(6,-6);
		
		List<Sommet> sommets = new ArrayList<Sommet>();
		List<Sommet> l = new ArrayList<Sommet>();
		
		sommets.add(sommet0);
		sommets.add(sommet1);
		sommets.add(sommet2);
		sommets.add(sommet3);
		sommets.add(sommet4);
		sommets.add(sommet5);
		sommets.add(sommet6);
		
		sommets.add(sommet7);
		sommets.add(sommet8);
		sommets.add(sommet9);
		sommets.add(sommet10);
		sommets.add(sommet11);
		sommets.add(sommet12);
		sommets.add(sommet13);
		sommets.add(sommet14);
		
		
		for(int i=0;i < nbSommets; i++) {
			l.add(sommets.get(i));
		}
		Polygone polygone = new Polygone(l);
		long startTime,stopTime,elapsedTime;
		System.out.printf("Nombre de sommets : %d\n",nbSommets);
		
		
		//Test l'algorithme essais successifs sans optimisation
		System.out.println("------------------- Test Essais Successifs sans optimisation ---------------------------");
		startTime = System.currentTimeMillis();
		polygone.essaisSuccessifs(0);//Appel essais successifs
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;		
		System.out.println("Temps utilisé : "+(double)elapsedTime/1000+" secondes");
		System.out.println("Résultat : Somme de longueurs optimale: "+polygone.getSomOpt());
		System.out.printf("Nombre de cordes tracées : %d\n Cordes tracées :",polygone.getY().size());
		for(Corde c:polygone.getY()) {
			System.out.printf("(s%d,s%d)", sommets.indexOf(c.getS1()),sommets.indexOf(c.getS2()));
		}
		System.out.println();
		System.out.println(polygone.getY().toString());
		
		
		
		//Test l'algorithme essais successifs avec l'optimisation
		System.out.println("-------------------- Test Essais Successifs avec optimisation ------------------------");
		startTime = System.currentTimeMillis();
		polygone.essaisSuccessifsOpt(0);//Appel essais successifs avec l'élagage
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Temps utilisé : "+(double)elapsedTime/1000+" secondes");
		System.out.printf("Résultat : ");
		System.out.println("Somme de longueurs optimale: "+polygone.getSomOpt());
		System.out.printf("Nombre de cordes tracées : %d\n Cordes tracées :",polygone.getY().size());
		for(Corde c:polygone.getY()) {
			System.out.printf("(s%d,s%d)", sommets.indexOf(c.getS1()),sommets.indexOf(c.getS2()));
		}
		System.out.println();
		System.out.println(polygone.getY().toString());
		
		
		//Test l'algorithme programmation dynamique
		System.out.println("---------------------- Test Programmation Dynamique --------------------");
		startTime = System.currentTimeMillis();
		polygone.dynamique();
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Temps utilisé : "+(double)elapsedTime/1000+" secondes");
		
		
		//Test l'algorithme glouton
		System.out.println("----------------------- Test Algorithme Glouton --------------------------");
		startTime = System.currentTimeMillis();
		polygone.glouton();
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Temps utilisé : "+(double)elapsedTime/1000+" secondes");
		System.out.println("---------------------------- Test finit ---------------------------------");
	}

}
