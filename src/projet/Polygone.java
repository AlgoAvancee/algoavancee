package projet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Polygone {
	/**
	 * Nombre de sommets.
	 */
	private int nbSommets;
	/**
	 * Liste des sommets du polygone.
	 */
	private List<Sommet> sommets;
	/**
	 * Liste de toutes les cordes possibles servant à méthode essais successifs.
	 */
	private List<Corde> cordes = new ArrayList<Corde>();
	/**
	 * Liste des cordes tracées servant à méthode essais successifs.
	 */
	private List<Corde> X;
	/**
	 * Somme courante des longueurs des cordes tracées servant à méthode essais successifs.
	 */
	private double somCour;
	/**
	 * Somme optimale des longueurs des cordes tracées servant à méthode essais successifs.
	 */
	private double somOpt;
	/**
	 * Liste de solutions complètes servant à méthode essais successifs.
	 */
	private List<Corde> Y;
	/**
	 * Constructeur par défaut, qui prend en paramètre la liste des sommets du polygone.
	 * @param _sommets
	 * 		Liste des sommets.
	 */
	public Polygone(List<Sommet> _sommets) {
		nbSommets = _sommets.size();
		sommets = _sommets;
		toutesCordes();
		X = new ArrayList<Corde>(nbSommets-3);
		somCour = 0;
		somOpt = Double.POSITIVE_INFINITY;
		Y = new ArrayList<Corde>();
	}
	/**
	 * Valider la nouvelle corde du polygone (sous la condition d'utiliser les attributs sommet de Corde mais pas indices de sommet)
	 * @param i
	 * @param j
	 * @return
	 */
	public boolean validecorde(int i, int j) {
		for(Corde c:cordes) {
			if(sommets.indexOf(c.getS1()) == i && sommets.indexOf(c.getS2()) == i ) {
				return validecorde(c);
			}
		}
		return false;
	}
	/**
	 * Valide la nouvelle corde du polygone à tracer 
	 * @param _corde nouvelle corde
	 * @return vrai si _corde n'est pas encore tracée et elle ne crois pas avec les autres cordes tracées sinon faux
	 */
	public boolean validecorde(Corde _corde) {
		if(nbSommets>3) {
			Sommet nouvS1 = _corde.getS1();
			Sommet nouvS2 = _corde.getS2();
			int i = sommets.indexOf(nouvS1);
			int j = sommets.indexOf(nouvS2);
			//Corde (si,sj) n'existe pas dans la liste
			if(!X.isEmpty() && !X.contains(_corde)) {
				//Corde (si,sj) ne coupe pas des autres cordes
				//Parcourir toutes les cordes existantes pour vérifier
					int indicex = cordes.indexOf(_corde);
					int indiceDernier = cordes.indexOf(X.get(X.size()-1));
					if(indicex<indiceDernier) {
						return false;
					}
					for(Corde corde:X) {
						//Obtenir l'index de sommet dans la liste de sommets
						//On suppose toujours que index1 < index2 et i < j
						int index1 = sommets.indexOf(corde.getS1());
						int index2 = sommets.indexOf(corde.getS2());
						boolean b = (i > index1 && i < index2 && j > index2)
								||
								(i < index1 && j > index1 && j < index2)
								//||
								//(index1 < i && index2 > i && index2 < j)
								;
						//les cordes ne se croisent pas
						if(b) return false;
					}
					return true;				
			}else {
			return true;
			}
		}
		return false;
	}
	/**
	 * Trouver toutes les cordes possibles du polygone
	 */
	public void toutesCordes() {
		int i,j;
		for(i = 0; i < nbSommets-2; i++) {
			for(j = i+2; j < nbSommets; j++) {
				if(! (i==0 && j==nbSommets-1)) {
					cordes.add(new Corde(sommets.get(i),sommets.get(j)));
				}
				
			}
		}
	}
	/**
	 * Essais successifs : Tracer les cordes dans le polygone et chercher la meilleur solution
	 * @param i ème étape à tracer la corde
	 */
	public void essaisSuccessifs(int i) {
		for(Corde x:cordes) {
			if(!x.isPris() && validecorde(x)) {
				X.add(x);
				x.setPris(true);
				somCour += x.getLongueur();
				//À l'étape (nbSommets-4) on trouve toutes les cordes valides
				if(i == nbSommets-4) {
					if(somCour < somOpt) {
						Y.removeAll(Y);
						Y.addAll(X);
						somOpt = somCour;
						//System.out.println("Essais successifs:somOpt:"+somOpt+Y.toString());
					}
				}else {
					if(i < (nbSommets-4)) {
						essaisSuccessifsOpt(i+1);
					}
				}
				x.setPris(false);
				somCour -=x.getLongueur();
				X.remove(x);
			}
		}
	}
	/**
	 * Essais successifs : Optimisation de tracer les cordes
	 * @param i ème étape à tracer la corde
	 */
	public void essaisSuccessifsOpt(int i) {
		for(Corde x:cordes) {
			if(!x.isPris() && validecorde(x)) {
				X.add(x);
				x.setPris(true);
				somCour += x.getLongueur();
				//Conditions d'élagage : 
				//1. La somme courante est déjà supérieure que la somme optimale
				//2. Il ne reste pas assez de cordes à parcourir pour former (nbSommets-3) cordes
				if(somCour < somOpt && (cordes.size()-i-1+X.size())>=(nbSommets-3)) {
					if(i == (nbSommets-4)) {
							Y.removeAll(Y);
							Y.addAll(X);
							somOpt = somCour;
					}else if(i < (nbSommets-4)){
							essaisSuccessifsOpt(i+1);
					}
				}
				x.setPris(false);
				somCour -=x.getLongueur();
				X.remove(x);
			}					
		}
	}
	/**
	 * Algorithme de triangulation de type programmation dynamique.
	 */
	public void dynamique() {
		//Structure tabulaire à deux dimensions, permettant d'enregistrer la distance de la triangulation minimale de chaque sous-problème.
		double[][] table = new double[nbSommets][nbSommets+1];
		//Liste de listes de cordes, permettant d'enregistrer l'ensemble des cordes nécessaires à la traingulation minimale de chaque sous-problème.
		List<List<Corde>> cordesDyn = new ArrayList<List<Corde>>();
		int i,t,k;
		//Initialisation des sous-problèmes dont le nombre de sommets est égal ou inférieur à 3 (il n'y a pas de cordes et la distance est de 0).
		for(i=0; i<nbSommets; i++) {
			for(t=0; t<4; t++) {
				table[i][t] = 0;
				cordesDyn.add(null);
			}
		}
		//Initialisation des distances des sous-problèmes dont le nombre de sommets est supérieur à 3.
		for(i=0; i<nbSommets; i++) {
			for(t=4; t<nbSommets+1; t++) {
				table[i][t] = Double.POSITIVE_INFINITY;
			}
		}
		//Calcul des sous-problèmes.
		//Remplisage du tableau des distances colonne par colonne, de gauche à droite.
		//i:0...n-1; t:4..n; k:1..t-1.
		for(t=4;t<nbSommets+1;t++) {
			for(i=0;i<nbSommets;i++) {
				//Variable temporaire permettant d'enregistrer l'ensemble des cordes de la meilleure triangulation actuellement trouvée.
				List<Corde> tmpCorde = new ArrayList<Corde>();

				for(k=1;k<t-1;k++) {
					//Cordes à tracer, et distances associées.
					Corde c1=null, c2=null;
					double d1, d2;
					//Variable temporaire permettant d'enregistrer la distance de la triangulation actuellement trouvée.
					double tmpDist;
					//Tracé des cordes selon la nature des sous-problèmes.
					if(k == 1 && k != (t-2)){
						d1 = 0;
						c2 = new Corde(sommets.get((i+k)%nbSommets),sommets.get((i+t-1)%nbSommets));
						d2 = c2.getLongueur();
					}else if(k != 1 && k == (t-2)){
						c1 = new Corde(sommets.get(i),sommets.get((i+k)%nbSommets));
						d1 = c1.getLongueur();
						d2 = 0;
					}else if(k == 1 && k == (t-2)) {
						d1 = 0;
						d2 = 0;
					}else{
						c1 = new Corde(sommets.get(i),sommets.get((i+k)%nbSommets));
						c2 = new Corde(sommets.get((i+k)%nbSommets),sommets.get((i+t-1)%nbSommets));
						d1 = c1.getLongueur();
						d2 = c2.getLongueur();
					}
					//Si la nouvelle distance enregistrée est la meilleure, on la sauvegarde ainsi que l'ensemble des cordes permettant d'arriver à ce résultat.
					//T[i,t] = min(d[i,i+k]+d[i+k,i+t-1]+T[i,k+1]+T[i+k,t-k])
					tmpDist = d1+d2+table[i][k+1]+table[(i+k)%nbSommets][t-k];
					if(tmpDist < table[i][t]) {
						table[i][t] = tmpDist;
						tmpCorde.clear();
						tmpCorde.add(c1);
						tmpCorde.add(c2);
						//Ajout des cordes du sous-problème T[i,k+1].
						if(cordesDyn.get((k+1)*nbSommets+i) != null) {
							tmpCorde.addAll(cordesDyn.get((k+1)*nbSommets+i));
						}
						//Ajout des cordes du sous-problème T[i+k,t-k].
						if(cordesDyn.get((t-k)*nbSommets+(i+k)%nbSommets) != null) {
							tmpCorde.addAll(cordesDyn.get((t-k)*nbSommets+(i+k)%nbSommets));
						}
					}
				}
				//Enregistrement des cordes permettant d'avoir la triangulation minimale du sous-problème traité.
				cordesDyn.add(tmpCorde);
			}
		}
		//Affichage du tableau des distances
		for(int m=0; m<nbSommets; m++) {
			for(int n=0; n<nbSommets+1; n++) {
				System.out.printf("%5f ",table[m][n]);
			}
			System.out.println();
		}
		//Suppression des éléments null de la liste des cordes permettant la triangulation du problème initial, pour n'avoir que des cordes valides dans cette dernière
		while(cordesDyn.get(nbSommets*nbSommets).contains(null)){
			cordesDyn.get(nbSommets*nbSommets).remove(null);
		}
		System.out.println("Nombre de cordes tracées : "+cordesDyn.get(nbSommets*nbSommets).size()+" "+cordesDyn.get(nbSommets*nbSommets).toString());
		System.out.printf("Cordes tracées : ");
		for(Corde c:cordesDyn.get(nbSommets*nbSommets)) {
			System.out.printf("(s%d,s%d)", sommets.indexOf(c.getS1()),sommets.indexOf(c.getS2()));
		}
		System.out.println("\nSomme des longueurs optimale: "+table[0][nbSommets]);
	}

	/**
	 * Algorithme de triangulation de type glouton.
	 */
	public void glouton() {
		List<Sommet> sommetsCopy = new ArrayList<Sommet>();
		List<Corde> cordesGlouton = new ArrayList<Corde>();
		sommetsCopy.addAll(sommets);
		Sommet s1 = null;
		Sommet s2 = null;
		int i, sommetsEnleves;
		double sommeLongueur = 0;
		List<Corde> cordeB = cordeBord(sommetsCopy);
		for(i=0;i<nbSommets-3;i++) {
			double l = Double.POSITIVE_INFINITY;//cette valeur stocke la longueur minimale
			
			for(Corde c : cordeB) {
				if(l>c.getLongueur()) {
					l = c.getLongueur();
					s1 = c.getS1();
					s2 = c.getS2();
				}
			}
			sommeLongueur += l;
			Corde tmp = new Corde(s1,s2);
			cordesGlouton.add(tmp);
			int indexS1 = sommetsCopy.indexOf(s1);
			int indexS2 = sommetsCopy.indexOf(s2);
			//Recherche le sommet à enlever 
			if(indexS1 == 0 && indexS2 == sommetsCopy.size()-2) {
				sommetsEnleves = sommetsCopy.size()-1;
			}else if(indexS1 == 1 && indexS2 == sommetsCopy.size()-1) {
				sommetsEnleves = 0;
			}else {
				sommetsEnleves = (indexS1+indexS2)/2;
			}
			sommetsCopy.remove(sommetsEnleves);//Enlève le sommet au milleu de la corde
			cordeB = cordeBord(sommetsCopy);//Recherche nouvelles cordes du bord de nouveau polygone qui a été enlevé un sommet
		}

		System.out.println("Nombre de cordes tracées : "+cordesGlouton.size()+" "+cordesGlouton.toString());
		System.out.println();
		for(Corde c:cordesGlouton) {
			System.out.printf("(s%d,s%d)", sommets.indexOf(c.getS1()),sommets.indexOf(c.getS2()));
		}
		System.out.println();
		System.out.println("Somme des longueurs optimale: "+sommeLongueur);

	}
	/**
	 * Cherche toutes les cordes possibles se situant au bord du polygone
	 * @return La liste de cordes du bord de polygone
	 */
	public List<Corde> cordeBord() {
		int i;
		List<Corde> cordeB = new ArrayList<Corde>();
		for(i = 0; i < nbSommets-2; i++) {
			cordeB.add(new Corde(sommets.get(i),sommets.get(i+2)));
		}
		if(nbSommets>4){
		cordeB.add(new Corde(sommets.get(0),sommets.get(nbSommets-2)));
		cordeB.add(new Corde(sommets.get(1),sommets.get(nbSommets-1)));
		}
		return cordeB;
	}
	/**
	 * Cherche toutes les cordes possibles se situant au bord du polygone
	 * @param s La liste de sommets constituant un polygone
	 * @return La liste de cordes du bord de polygone
	 */
	public List<Corde> cordeBord(List<Sommet> s) {
		int i;
		List<Corde> cordeB = new ArrayList<Corde>();
		for(i = 0; i < s.size()-2; i++) {
			cordeB.add(new Corde(s.get(i),s.get(i+2)));
		}
		if(s.size()>4){
			cordeB.add(new Corde(s.get(0),s.get(s.size()-2)));
			cordeB.add(new Corde(s.get(1),s.get(s.size()-1)));
		}
		return cordeB;
	}
	public int getNbSommets() {
		return nbSommets;
	}

	public void setNbSommets(int nbSommets) {
		this.nbSommets = nbSommets;
	}

	public List<Sommet> getSommets() {
		return sommets;
	}

	public void setSommets(List<Sommet> sommets) {
		this.sommets = sommets;
	}

	public List<Corde> getCordes() {
		return cordes;
	}

	public void setCordes(List<Corde> cordes) {
		this.cordes = cordes;
	}

	public double getSomOpt() {
		return somOpt;
	}

	public void setSomOpt(double somOpt) {
		this.somOpt = somOpt;
	}
	/**
	 * Obtenir la solution optimale par la méthode d'essais successifs
	 * @return
	 */
	public List<Corde> getY() {
		return Y;
	}
	public void setY(List<Corde> y) {
		Y = y;
	}

}