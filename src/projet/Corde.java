package projet;

public class Corde {
	/**
	 * Une corde possède 2 sommets.
	 */
	private Sommet s1,s2;
	/**
	 * Indique si la corde est prise dans la triangulation du polygone.
	 */
	private boolean pris;
	/**
	 * Longueur de la corde.
	 */
	private double longueur;

	/**
	 * Constructeur par défaut.
	 */
	public Corde() {
	}

	/**
	 * Constructeur qui prend en paramètre les deux sommets de la corde.
	 * @param _s1 
	 * 		Sommet 1.
	 * @param _s2
	 * 		Sommet 2.
	 */
	public Corde(Sommet _s1, Sommet _s2) {
		this.s1 = _s1;
		this.s2 = _s2;
		pris = false;
		longueur = calculLongueur();
	}

	/**
	 * Calcul de la longueur de la corde.
	 * @return longueur de la corde.
	 */
	public double calculLongueur() {
		return Math.sqrt(Math.pow((s1.getAbscisse()-s2.getAbscisse()), 2)+Math.pow((s1.getOrdonnee()-s2.getOrdonnee()), 2));
	}
	
	public Sommet getS1() {
		return s1;
	}

	public void setS1(Sommet s1) {
		this.s1 = s1;
	}

	public Sommet getS2() {
		return s2;
	}

	public void setS2(Sommet s2) {
		this.s2 = s2;
	}

	public boolean isPris() {
		return pris;
	}

	public void setPris(boolean pris) {
		this.pris = pris;
	}

	public double getLongueur() {
		return longueur;
	}

	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Corde))
			return false;
		if(obj instanceof Corde) {
			Corde other = (Corde) obj;
			if((s1.equals(other.getS1()) && s2.equals(other.getS2()))
					|| (s1.equals(other.getS2()) && s2.equals(other.getS1())) 
					)
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Corde [s1=" + s1 + ", s2=" + s2 + "]";
	}


}
