package projet;

public class Sommet {
	/**
	 * La coordonnée de sommet
	 */
	private int abscisse,ordonnee; 
	
	public Sommet(){
		abscisse = 0;
		ordonnee = 0;
	}
	
	public Sommet(int x, int y) {
		this.abscisse = x;
		this.ordonnee = y;
	}

	public int getAbscisse() {
		return abscisse;
	}

	public void setAbscisse(int abscisse) {
		this.abscisse = abscisse;
	}

	public int getOrdonnee() {
		return ordonnee;
	}

	public void setOrdonnee(int ordonnee) {
		this.ordonnee = ordonnee;
	}

	@Override
	public String toString() {
		return "Sommet [x=" + abscisse + ", y=" + ordonnee + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Sommet))
			return false;
		Sommet other = (Sommet) obj;
		return (abscisse==other.abscisse) && (ordonnee==other.ordonnee);
	}
}
